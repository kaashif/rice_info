# rice_info

![example.png](example.png)

This is a program that lets you write notes on stuff you've riced in a
standardised format. It's just a copy of
[ricerous](https://github.com/nixers-projects/ricerous) except,
personally, I think the UI of this is better. I've never been a fan of
non-native looking GUIs. This one is at least possibly slightly
native-looking.

## Installation

You need PyQt5. Your favourite OS probably has a package for it.

Try one of these:

```bash
$ sudo apt install python3-pyqt5
$ sudo pkg_add py3-qt5
$ sudo dnf install PyQt5
$ sudo pkg install py-qt5
```

Then run:

```bash
$ sudo python3.5 setup.py install
```

And you're good to go:

```bash
$ rice_info
```
