def output(state):
    out = "<h1>Rice Info Output</h1>\n"
    for cat in state.state["selections"]:
        out += "<h2>{}</h2>\n".format(cat)
        out += "<p>{}</p>\n".format(state["comments"][cat])
    return out
