def output(state):
    out = "# Rice Info Output\n"
    for cat in state.state["selections"]:
        out += "##{}\n".format(cat)
        out += "{}\n".format(state.state["comments"][cat])
    return out
