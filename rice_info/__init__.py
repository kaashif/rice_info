import sys, inspect
from rice_info.state import RiceState
from rice_info.plugins import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *

class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.initUI()
        
    def initUI(self):               
        self.state = RiceState()
        self.cur_category = "TTY"
        menubar = self.menuBar()
        fileMenu = menubar.addMenu("&File")

        importAction = QAction(QIcon(""), "Import from...", self)
        importAction.setStatusTip("Import the current rice info from a file")
        fileMenu.addAction(importAction)

        exportMenu = fileMenu.addMenu("Export as...")
        exportMenu.setStatusTip("Export the current rice info to a file")
        for plugin,plugin_module in inspect.getmembers(plugins, inspect.ismodule):
            pluginAction = QAction(QIcon(""), "Export as {}".format(plugin), self)
            pluginAction.setStatusTip("Export as {}".format(plugin))
            pluginAction.triggered.connect(lambda: self.file_dialog_call(lambda fname: self.export_using(plugin_module, fname)))
            exportMenu.addAction(pluginAction)
        fileMenu.addMenu(exportMenu)

        fileMenu.addSeparator()

        exitAction = QAction(QIcon(""), "Exit", self)
        exitAction.setStatusTip("Exit Rice Info")
        exitAction.triggered.connect(self.close)
        fileMenu.addAction(exitAction)

        helpMenu = menubar.addMenu("&Help")

        aboutAction = QAction(QIcon(""), "About Rice Info", self)
        aboutAction.triggered.connect(self.about)
        helpMenu.addAction(aboutAction)

        mainbox = QHBoxLayout()
        textboxes = QVBoxLayout()

        infobox = QLabel()
        textboxes.addWidget(infobox)

        sidebar = QListWidget()
        sidebar.setMaximumWidth(200)
        sidebar.itemClicked.connect(lambda category: self.update_text(category.text(), infobox, inputbox))
        
        inputbox = QTextEdit()
        textboxes.addWidget(inputbox)
        mainbox.addWidget(sidebar)
        mainbox.addLayout(textboxes)

        for category,comment in self.state.state["comments"].items():
            sidebar.addItem(category)

        mainwidget = QWidget()
        mainwidget.setLayout(mainbox)

        self.setCentralWidget(mainwidget)
        self.statusBar().showMessage('Ready')
        self.setGeometry(300, 300, 250, 150)

        self.update_text(self.cur_category, infobox, inputbox, False)
        self.show()

    def export_using(self, plugin, fname):
        print(str(plugin))
        with open(fname, "w") as f:
            f.write(plugin.output(self.state))
                

    def update_text(self, new_category, infobox, inputbox, change_state=True):
        if change_state:
            self.state.state["comments"][self.cur_category] = inputbox.toPlainText()
        self.cur_category = new_category
        inputbox.setText(self.state.state["comments"][self.cur_category])
        infobox.setText(self.state.info[self.cur_category])

    def file_dialog_call(self, func):
        fname = QFileDialog.getSaveFileName()[0]
        if fname != "":
            func(fname)

    def about(self):
        QMessageBox.information(self, "About Rice Info", "rice_info 0.0.1\n\nCopyright (c) 2016 Kaashif Hymabaccus")
        

def main():
    app = QApplication(sys.argv)
    mainwindow = MainWindow()
    sys.exit(app.exec_())
    
    
