import json
from collections import OrderedDict
from pkg_resources import resource_filename

class RiceState(object):
    def __init__(self):
        self.state = self.import_from(resource_filename("rice_info", "resources/defaultstate.json"))
        self.info = self.import_from(resource_filename("rice_info", "resources/info_text.json"))

    def export_to(self, path):
        with open(path, "w") as out:
            json.dump(self.state, out)

    def import_from(self, path):
        with open(path, "r") as inp:
            return json.load(inp, object_pairs_hook=OrderedDict)
