#!/usr/bin/env python3
import os
from setuptools import setup

setup(
    name = "rice_info",
    version = "0.0.1",
    author = "kaashif",
    description = ("create structured, easy to read info on your rice"),
    license = "BSD",
    packages = ["rice_info", "rice_info.plugins"],
    scripts = ["scripts/rice_info"],
    install_requires = [
        "PyQt5",
    ],
    package_data = {
        "rice_info": [
            "resources/defaultstate.json",
            "resources/info_text.json"
        ]
    }
)
